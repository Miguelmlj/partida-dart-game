import 'package:partida/partida.dart';
import 'package:partida/src/puntuaciones.dart';

import 'package:test/test.dart';
//import '';

void main() {
    group('Partidas', (){
      late Jugador j1,j2,j3,j4,j5, jRepetido;
      setUp((){
        j1 = Jugador(nombre:'juan');
        j2 = Jugador(nombre:'Pancho');
        j3 = Jugador(nombre:'Paco');
        jRepetido = Jugador(nombre:'Paco');
        j4 = Jugador(nombre:'Maria');
        j5 = Jugador(nombre:'pepe');
      });
      test('debe de tener al menos dos jugadores', (){
      expect(()=>Partida(jugadores:{j1}), throwsA(TypeMatcher<ProblemaNumeroJugadoresMenorMinimo>()));
    });

    test('debe de tener maximo 4 jugadores',(){
      expect(() => Partida(jugadores:{j1,j2,j3,j4,j5}),
        throwsA(TypeMatcher<ProblemaNumeroJugadoresMayorMaximo>()));

    });

    test('con dos jugadores está bien', (){
      expect(()=> Partida(jugadores:{j1,j2}), returnsNormally);
    });


  });
    
    group('Puntuaciones Ronda 1', (){
      late Jugador j1,j2,j3;
      late PRonda1 p1,p2,p3;

      setUp((){
        j1 = Jugador(nombre: 'Pancho');
        j2 = Jugador(nombre: 'Paco');
        j3 = Jugador(nombre: 'Pedro');

         p1 = PRonda1(jugador: j1, cuantasAzules: 3);
         p2 = PRonda1(jugador: j2, cuantasAzules: 6);
         p3 = PRonda1(jugador: j3, cuantasAzules: 5);
      });
      test('Jugadores diferentes no se debe', (){
        Partida p = Partida(jugadores:{j1,j2});
        expect(() => p.puntuacionRonda1([p1,p3]), throwsA(TypeMatcher<ProblemaJugadoresNoConcuerdan>()));

        
      });

      test('Jugadores deben concordar', (){
        Partida p = Partida(jugadores: {j1,j2});
        expect(()=>p.puntuacionRonda1([p1,p2]), returnsNormally);

      });

    });

    group('Puntuaciones Ronda 2', (){
      late Jugador j1, j2, j3;
      late PRonda2 p21, p22, p23;
      late PRonda1 p11,p12,p13;
      late PRonda1 p11mal,p12mal;
      setUp((){
        j1 = Jugador(nombre: 'Pancho');
        j2 = Jugador(nombre: 'Paco');
        j3 = Jugador(nombre: 'Pedro');

        p11 = PRonda1(jugador: j1, cuantasAzules: 0);
        p12 = PRonda1(jugador: j2, cuantasAzules: 0);
        p13 = PRonda1(jugador: j3, cuantasAzules: 7);

        p11mal = PRonda1(jugador: j1, cuantasAzules: 10);
        p12mal = PRonda1(jugador: j2, cuantasAzules: 10);

        p21 = PRonda2(jugador: j1, cuantasAzules: 1, cuantasVerdes:1);
        p22 = PRonda2(jugador: j2, cuantasAzules: 2, cuantasVerdes:2);
        p23 = PRonda2(jugador: j3, cuantasAzules: 1, cuantasVerdes:1);

      });

      test('Debe ser llamada después de puntuacion ronda 1', (){
        Partida p = Partida(jugadores: {j1,j2});
        expect(() => p.puntuacionRonda2([p21,p23]), throwsA(TypeMatcher<ProblemaOrdenIncorrecto>()));
      });

      test('Jugadores diferentes manda error', (){
        Partida p = Partida(jugadores: {j1,j2});
        p.puntuacionRonda1([p11,p12]);
        expect(() => p.puntuacionRonda2([p21,p23]), throwsA(TypeMatcher<ProblemaJugadoresNoConcuerdan>()));
      });

      test('Jugadores si concuerdan', (){
        Partida p = Partida(jugadores: {j1,j2});
        p.puntuacionRonda1([p11,p12]);
        expect(() => p.puntuacionRonda2([p21,p22]), returnsNormally);
      });


      test('Cartas azules no deben ser menores a el número anterior', () {
        Partida p = Partida(jugadores:{j1,j2});
        p.puntuacionRonda1([p11mal,p12mal]);
        expect(()=>p.puntuacionRonda2([p21,p22]), throwsA(TypeMatcher<ProblemaDisminucionAzules>()));
        
      });

      test('Cartas azules pueden ser iguales o mayores al numero anterior', () {
        Partida p = Partida(jugadores: {j1,j2});
        p.puntuacionRonda1([p11,p12]);
        expect(()=>p.puntuacionRonda2([p21,p22]), returnsNormally);

      });

      test('Cartas azules deben ser iguales o mayores', () {
        
        Partida p = Partida(jugadores: {j1,j2});
        p.puntuacionRonda1([p11,p12]);
        expect(()=>p.puntuacionRonda2([p21,p22]), returnsNormally);

      });

      test('Maximo de cartas azules es 20', () {
        Partida p = Partida(jugadores: {j1,j2});
        p.puntuacionRonda1([p11,p12]);
        expect(()=>p.puntuacionRonda2([PRonda2(jugador: j1, cuantasAzules: 21, cuantasVerdes:0),
        PRonda2(jugador: j2, cuantasAzules: 0, cuantasVerdes: 0)]), 
        throwsA(TypeMatcher<ProblemaDemasiadasAzules>()));
      });

      test('Maximo de cartas verdes es 20', (){
        Partida p = Partida(jugadores: {j1,j2});
        p.puntuacionRonda1([p11,p12]);
        expect(()=>p.puntuacionRonda2([PRonda2(jugador: j1, cuantasAzules:0, cuantasVerdes:30),PRonda2(jugador: j2, cuantasAzules: 0, cuantasVerdes: 0)]), throwsA(TypeMatcher<ProblemaDemasiadasVerdes>()));

      });

      test('Maximo de ambas cartas debe de ser 20', (){

        Partida p = Partida(jugadores: {j1,j2});
        p.puntuacionRonda1([p11,p12]);
        expect(()=>p.puntuacionRonda2([PRonda2(jugador: j1, cuantasAzules:11, cuantasVerdes:11),PRonda2(jugador: j2, cuantasAzules: 0, cuantasVerdes: 0)]), throwsA(TypeMatcher<ProblemaExcesoCartas>()));


      });

    });

    group('Puntuaciones Ronda 3', () {

      late Jugador j1,j2,j3;
      late PRonda3 p31,p32,p33;
      late PRonda2 p21,p22,p23;
      late PRonda2 p21mal,p22mal,p23mal,p24mal;
      late PRonda1 p11,p12;

      setUp((){

        j1 = Jugador(nombre: 'player1');
        j2 = Jugador(nombre: 'player2');
        j3 = Jugador(nombre: 'player3');

        p11 = PRonda1(jugador: j1, cuantasAzules: 0);
        p12 = PRonda1(jugador: j2, cuantasAzules: 0);

        p21 = PRonda2(jugador: j1, cuantasAzules: 0, cuantasVerdes: 0);
        p22 = PRonda2(jugador: j2, cuantasAzules: 0, cuantasVerdes: 0);
        p23 = PRonda2(jugador: j3, cuantasAzules: 0, cuantasVerdes: 0);

        p21mal = PRonda2(jugador: j1, cuantasAzules: 10, cuantasVerdes: 0);
        p22mal = PRonda2(jugador: j2, cuantasAzules: 10, cuantasVerdes: 0);
        p23mal = PRonda2(jugador: j1, cuantasAzules: 0, cuantasVerdes: 10);
        p24mal = PRonda2(jugador: j2, cuantasAzules: 0, cuantasVerdes: 10);

        p31 = PRonda3(jugador: j1, cuantasAzules: 1, cuantasVerdes: 1, cuantasRosas: 0, cuantasNegras: 0);
        p32 = PRonda3(jugador: j2, cuantasAzules: 2, cuantasVerdes: 2, cuantasRosas: 0, cuantasNegras: 0);
        p33 = PRonda3(jugador: j3, cuantasAzules: 1, cuantasVerdes: 1, cuantasRosas: 0, cuantasNegras: 0);

        
      });

      test('Debe ser llamada después de puntuacion ronda 2', () {
        Partida p = Partida(jugadores: {j1,j2});
        expect(()=> p.puntuacionRonda3([p31,p33]), throwsA(TypeMatcher<ProblemaOrdenIncorrecto>()));
      });

      test('jugadores diferentes manda error', () {
        Partida p = Partida(jugadores: {j1,j2});
        p.puntuacionRonda1([p11,p12]);
        p.puntuacionRonda2([p21,p22]);

        expect(()=>p.puntuacionRonda3([p31,p33]), throwsA(TypeMatcher<ProblemaJugadoresNoConcuerdan>()));

      });

      test('jugadores si concuerdan es correcto', () {
        Partida p = Partida(jugadores: {j1,j2});  
        p.puntuacionRonda1([p11,p12]);
        p.puntuacionRonda2([p21,p22]);

        expect(()=>p.puntuacionRonda3([p31,p32]), returnsNormally);

      });

      test('Cartas azules no deben ser menores', () {
        Partida p = Partida(jugadores: {j1,j2});
        p.puntuacionRonda1([p11,p12]);
        p.puntuacionRonda2([p21mal,p22mal]);
        expect(()=>p.puntuacionRonda3([p31,p32]), throwsA(TypeMatcher<ProblemaDisminucionAzules>()));
        
      });

      test('Cartas azules deben ser iguales o mayores', () {
        Partida p = Partida(jugadores: {j1,j2});
        p.puntuacionRonda1([p11,p12]);
        p.puntuacionRonda2([p21,p22]);
        expect(()=>p.puntuacionRonda3([p31,p32]), returnsNormally);
        
      });

      test('Cartas verdes no deben ser menores', () {
        Partida p = Partida(jugadores: {j1,j2});
        p.puntuacionRonda1([p11,p12]);
        p.puntuacionRonda2([p23mal,p24mal]);
        expect(()=>p.puntuacionRonda3([p31,p32]), throwsA(TypeMatcher<ProblemaDisminucionVerdes>()));
        
      });

      test('Cartas verdes deben iguales o mayores', () {
        Partida p = Partida(jugadores: {j1,j2});
        p.puntuacionRonda1([p11,p12]);
        p.puntuacionRonda2([p21,p22]);
        expect(()=>p.puntuacionRonda3([p31,p32]), returnsNormally);
        
      });

      test('Maximo de cartas azules es 30', () {
        Partida p = Partida(jugadores: {j1,j2});
        p.puntuacionRonda1([p11,p12]);
        p.puntuacionRonda2([p21,p22]);
        expect(()=>p.puntuacionRonda3([PRonda3(jugador: j1, cuantasAzules: 31, cuantasVerdes: 0, cuantasRosas: 0, cuantasNegras: 0),PRonda3(jugador: j2, cuantasAzules: 0, cuantasVerdes: 0, cuantasRosas: 0, cuantasNegras: 0)]),throwsA(TypeMatcher<ProblemaDemasiadasAzules>()));

        
      });

      test('Maximo de cartas verdes es 30', () {
        Partida p = Partida(jugadores: {j1,j2});
        p.puntuacionRonda1([p11,p12]);
        p.puntuacionRonda2([p21,p22]);
        expect(()=>p.puntuacionRonda3([PRonda3(jugador: j1, cuantasAzules: 0, cuantasVerdes: 31, cuantasRosas: 0, cuantasNegras: 0), PRonda3(jugador: j2, cuantasAzules: 0, cuantasVerdes: 0, cuantasRosas: 0, cuantasNegras: 0)]), throwsA(TypeMatcher<ProblemaDemasiadasVerdes>()));
      });

      test('Maximo de cartas negras es 30', () {
        Partida p = Partida(jugadores: {j1,j2});
        p.puntuacionRonda1([p11,p12]);
        p.puntuacionRonda2([p21,p22]);
        expect(()=>p.puntuacionRonda3([PRonda3(jugador: j1, cuantasAzules: 0, cuantasVerdes: 0, cuantasRosas: 0, cuantasNegras: 31), PRonda3(jugador: j2, cuantasAzules: 0, cuantasVerdes: 0, cuantasRosas: 0, cuantasNegras: 0)]), throwsA(TypeMatcher<ProblemaDemasiadasNegras>()));
      });

      test('Maximo de cartas rosas es 30', () {
        Partida p = Partida(jugadores: {j1,j2});
        p.puntuacionRonda1([p11,p12]);
        p.puntuacionRonda2([p21,p22]);
        expect(()=>p.puntuacionRonda3([PRonda3(jugador: j1, cuantasAzules: 0, cuantasVerdes: 0, cuantasRosas: 31, cuantasNegras: 0), PRonda3(jugador: j2, cuantasAzules: 0, cuantasVerdes: 0, cuantasRosas: 0, cuantasNegras: 0)]), throwsA(TypeMatcher<ProblemaDemasiadasRosas>()));
      });


      test('Máximo de sumas de cartas debe de ser 30', () {
        Partida p = Partida(jugadores: {j1,j2});
        p.puntuacionRonda1([p11,p12]);
        p.puntuacionRonda2([p21,p22]);
        expect(()=>p.puntuacionRonda3([PRonda3(jugador: j1, cuantasAzules:10, cuantasVerdes:10, cuantasRosas: 10, cuantasNegras: 10),PRonda3(jugador: j2, cuantasAzules: 0, cuantasVerdes: 0, cuantasRosas: 0, cuantasNegras: 0)]), throwsA(TypeMatcher<ProblemaExcesoCartas>()));  

      });
    });

    group('Puntuaciones', (){
      
      late Jugador j1,j2;
      late PRonda1 p1,p2;
      late PRonda2 p21,p22;
      late PRonda3 p31,p32;

      setUp((){
        j1 = Jugador(nombre: 'Pancho');
        j2 = Jugador(nombre: 'Paco');

         p1 = PRonda1(jugador: j1, cuantasAzules: 3);
         p2 = PRonda1(jugador: j2, cuantasAzules: 0);

         p21 = PRonda2(jugador: j1, cuantasAzules: 3, cuantasVerdes: 3);
         p22 = PRonda2(jugador: j2, cuantasAzules: 0, cuantasVerdes: 0);

         p31 = PRonda3(jugador: j1, cuantasAzules: 3, cuantasVerdes: 3, cuantasRosas: 1, cuantasNegras: 1);
         p32 = PRonda3(jugador: j2, cuantasAzules: 0, cuantasVerdes: 0, cuantasRosas: 1, cuantasNegras: 1);
      });
      
      

      test('Debe regresar una lista con PuntuacionJugador .ronda1', () {
        Partida p = Partida(jugadores: {j1,j2});
        p.puntuacionRonda1([p1,p2]);
        expect(()=>p.puntuaciones(FasePuntuacion.ronda1), returnsNormally);
      });


      test('Debe regresar una lista con PuntuacionJugador .ronda2', () {
        Partida p = Partida(jugadores: {j1,j2});
        p.puntuacionRonda1([p1,p2]);
        p.puntuacionRonda2([p21,p22]);

        expect(()=>p.puntuaciones(FasePuntuacion.ronda2), returnsNormally);
      });


      test('Debe regresar una lista con PuntuacionJugador .ronda3', () {
        Partida p = Partida(jugadores: {j1,j2});
        p.puntuacionRonda1([p1,p2]);
        p.puntuacionRonda2([p21,p22]);
        p.puntuacionRonda3([p31,p32]);

        expect(()=>p.puntuaciones(FasePuntuacion.ronda3), returnsNormally);
      });


      test('Debe regresar una lista con PuntuacionJugadorFinal .desenlace', () {
        Partida p = Partida(jugadores: {j1,j2});
        p.puntuacionRonda1([p1,p2]);
        p.puntuacionRonda2([p21,p22]);
        p.puntuacionRonda3([p31,p32]);

        expect(()=>p.puntuaciones(FasePuntuacion.desenlace), returnsNormally);
      });

     

    });

}

//probar las puntuacoines ronbda1
        //crear un juego
        // darle numero de cartas azules al jugador 1, digamos 3
        // darle numero de cartas azules al jugador 2, asignar 0
        // debemos llamar el método de puntuaciones
        // me debe regresar una lista con PuntuacionesJugador
        // [Jugador1, 9,0,0]  35 1 

      //probar las puntuacoines ronbda2
      //probar las puntuacoines ronbda3
      //probar las puntuacoines fianles


      // late PRonda3 p31,p32,p33;
      // late PRonda2 p21,p22,p23;
      // late PRonda2 p21mal,p22mal,p23mal,p24mal;
      // late PRonda1 p11,p12;



// p11 = PRonda1(jugador: j1, cuantasAzules: 0);
        // p12 = PRonda1(jugador: j2, cuantasAzules: 0);

        // p21 = PRonda2(jugador: j1, cuantasAzules: 0, cuantasVerdes: 0);
        // p22 = PRonda2(jugador: j2, cuantasAzules: 0, cuantasVerdes: 0);
        // p23 = PRonda2(jugador: j3, cuantasAzules: 0, cuantasVerdes: 0);

        // p21mal = PRonda2(jugador: j1, cuantasAzules: 10, cuantasVerdes: 0);
        // p22mal = PRonda2(jugador: j2, cuantasAzules: 10, cuantasVerdes: 0);
        // p23mal = PRonda2(jugador: j1, cuantasAzules: 0, cuantasVerdes: 10);
        // p24mal = PRonda2(jugador: j2, cuantasAzules: 0, cuantasVerdes: 10);

        // p31 = PRonda3(jugador: j1, cuantasAzules: 1, cuantasVerdes: 1, cuantasRosas: 0, cuantasNegras: 0);
        // p32 = PRonda3(jugador: j2, cuantasAzules: 2, cuantasVerdes: 2, cuantasRosas: 0, cuantasNegras: 0);
        // p33 = PRonda3(jugador: j3, cuantasAzules: 1, cuantasVerdes: 1, cuantasRosas: 0, cuantasNegras: 0);



























//Test ronda3
// en el orden correcto -> done

//si concuerdan jugadores -> done

//azules no deben de ser menores -> done

//verdes no deben ser menores -> done

//la suma total no debe pasar de 30 -> done

//las cantidades individuales (de negras,verdes,azules,rosas no deben ser mas de 30)   ->done

//1 17 