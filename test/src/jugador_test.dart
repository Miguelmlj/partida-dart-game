import 'package:test/test.dart';
import 'package:partida/partida.dart';

void main(){
  group('Jugador',(){
    test('debe de tener nombre no vacío', (){
      expect(() => Jugador(nombre:''),
      throwsA(TypeMatcher<ProblemaNombreJugadorVacio>()));
    });

    test('mismo nombre, mismo id',(){
      Jugador j1 = Jugador(nombre: 'paco');
      Jugador j2 = Jugador(nombre: 'paco');
      expect(j1, equals(j2));
    });

    test('diferente nombre, diferentes instancias',(){
      Jugador j1 = Jugador(nombre: 'paco');
      Jugador j2 = Jugador(nombre: 'Juan');
      expect(j1, isNot(equals(j2)));
    });

  });
}

